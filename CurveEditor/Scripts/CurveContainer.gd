extends Control

const padding := Vector2(50, 50)
var render: bool = false

func _draw() -> void:
	UpdateSize()
	if !render: return
	
	for path in Global.Paths.values():
		if path.Path != null:
			var curve = path.Path.curve
			
			if path.Visible:
				if curve.get_point_count() > 1:
					draw_polyline(curve.get_baked_points(),
					path.Color, path.Width ,path.AA)

func UpdateSize() -> void:
	var furthest_point := Vector2.ZERO
	for path in get_children():
		for point in path.get_children():
			if point.position.x > furthest_point.x:
				furthest_point.x = point.position.x
			if point.position.y > furthest_point.y:
				furthest_point.y = point.position.y
	set_size(furthest_point + padding)
