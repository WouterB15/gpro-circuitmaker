extends HBoxContainer

signal VisibilityToggled(id: int, toggled: bool)
signal DeletePressed(id: int)
signal Selected(id: int, toggled: bool)
signal MetaDataChanged

@onready var eye_icon = load("res://Images/CurveIcons/eye.svg")
@onready var eye_slash_icon = load("res://Images/CurveIcons/eye_slash.svg")

#Properties
var Id: int = -1
var Name: String

var popup: Popup

func _ready() -> void:
	$cols/row_1/pathName.grab_focus()
	$Selected.button_pressed = true
	
	popup = $cols/row_2/Color.get_popup()
	popup.visibility_changed.connect(set_popup_pos)

func set_metadata(id: int) -> void:
	var data = Global.Paths[id]
	Id = id
	set_path_name(data.Name)
	set_path_visibility(data.Visible)
	set_width(data.Width)
	set_color(data.Color)
	set_aa(data.AA)


func set_path_name(new_name: String) -> void:
	$cols/row_1/pathName.text = new_name

func set_path_visibility(toggled: bool) -> void:
	$cols/row_1/Visibility.button_pressed = !toggled

func set_color(col: Color) -> void:
	$cols/row_2/Color.color = col

func set_width(px: int) -> void:
	$cols/row_2/Thickness.value = px

func set_aa(toggled: bool) -> void:
	$cols/row_2/AA.button_pressed = toggled

func select() -> void:
	$Selected.button_pressed = true

func _on_path_name_text_changed(new_text: String) -> void:
	Name = new_text
	Global.Paths[Id].Name = new_text
	emit_signal("MetaDataChanged")

func _on_path_name_text_submitted(new_text: String) -> void:
	$cols/row_1/pathName.release_focus()
	Name = new_text
	Global.Paths[Id].Name = new_text
	emit_signal("MetaDataChanged")

func _on_visibility_toggled(button_pressed: bool) -> void:
	select()
	if !button_pressed:
		$cols/row_1/Visibility.icon = eye_icon
	if button_pressed:
		$cols/row_1/Visibility.icon = eye_slash_icon
	
	emit_signal("VisibilityToggled", Id, button_pressed)

func _on_delete_pressed() -> void:
	emit_signal("DeletePressed", Id)

func _on_selected_toggled(toggled: bool) -> void:
	if !toggled:
		return
	emit_signal("Selected", Id)

func _on_thickness_value_changed(value: float) -> void:
	select()
	Global.Paths[Id].Width = value
	emit_signal("MetaDataChanged")

func _on_color_changed(color: Color) -> void:
	Global.Paths[Id].Color = color
	emit_signal("MetaDataChanged")

func _on_aa_toggled(button_pressed: bool) -> void:
	Global.Paths[Id].AA = button_pressed
	emit_signal("MetaDataChanged")

func set_popup_pos() -> void:
	popup.position = Vector2(global_position.x-popup.size.x, global_position.y)
	if popup.position.y + popup.size.y > get_viewport_rect().size.y:
		popup.position.y = get_viewport_rect().size.y - popup.size.y




