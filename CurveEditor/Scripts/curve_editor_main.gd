extends Control

enum {NONE = -1, MOVE, ADD, DELETE, CURVE}

signal StateChanged(State)

@export_node_path(Node) var PathContainer

@onready var handle_scene = load("res://CurveEditor/curve_nodes/curve_main.tscn")
@onready var line_scene = load("res://CurveEditor/NewPath.tscn")
@onready var eye_icon = load("res://Images/CurveIcons/eye.svg")
@onready var eye_slash_icon = load("res://Images/CurveIcons/eye_slash.svg")


@onready var PathCont = get_node(PathContainer)
@onready var tree: Control = $Inspector/vbox/scroll/cont

var editing_id: int
var editing_path: Path2D

var state
var should_place: bool = true

#Call update on the path container to draw lines
func UpdatePaths() -> void:
	PathCont.update()

#Input handling
func _unhandled_input(event: InputEvent) -> void:
	handle_add(event)

#Method that handles when a user wants to add a node
func handle_add(event: InputEvent) -> void:
	#Fist check for mousemotion, we don't want to add a node 
	#if the user is trying to move the camera
	if event is InputEventMouseMotion:
		should_place = false
		return
	
	#If we detect a click, check if there was mouse motion leading it
	#If so, don't place, but do reset the state management
	if event is InputEventMouseButton and state == ADD:
		if !should_place:
			should_place = true
			return
		
		#Various checks to make sure we can safely place
		if event.is_pressed(): return
		if event.button_index != MOUSE_BUTTON_LEFT: return
		if editing_path == null: return
		if Global.Paths[editing_id].Closed:
			Global.SendAlert("Cannot add points if the path is closed.
							Please open the path to add more points",
							Global.AlertType.WARNING)
			return
		
		#Set the position and name of the new handle
		var pos = PathCont.get_global_mouse_position().floor()
		CreateHandle(pos)

func CreateHandle(pos: Vector2):
	var handle = handle_scene.instantiate()
	handle.name = str(editing_path.curve.get_point_count())
	handle.global_position = pos
	
	#Connect the signals from and to this script
	ConnectHandleSignals(handle)
	
	#Add the handle to the scenetree
	editing_path.add_child(handle)
	editing_path.curve.add_point(pos)
	
	#Set the curve handles to aim towards the next/previous node
	if handle.name != "0":
		#Get previous handle and the direction vector to it
		var prev_handle = editing_path.get_child(str(handle.name).to_int() - 1)
		var dir = (prev_handle.position - handle.position).normalized()
		
		#Set the in-handle position of the new handle
		var in_handle = handle.get_node("in")
		in_handle.position = dir * Vector2(50, 50)
		
		#Set the out-handle position of the previous handle
		var out_handle = prev_handle.get_node("out")
		out_handle.position = -dir * Vector2(50, 50)
		
	UpdatePaths()
	return handle

#Method that creates an item in the path editor sidebar
func CreateTreeItem() -> void:
	#Instance and name the tree item
	var child = line_scene.instantiate()
	child.Id = child.get_instance_id()
	child.name = str(child.Id)
	
	#Connect the signals to this script
	child.VisibilityToggled.connect(_on_visibility_toggled)
	child.DeletePressed.connect(_on_delete_pressed)
	child.Selected.connect(_on_path_selected)
	child.MetaDataChanged.connect(UpdatePaths)
	
	#Set the default metadata for the path
	var new_path := {}
	new_path["Name"] = ""
	new_path["Path"] = Path2D.new()
	new_path["Visible"] = true
	new_path["Width"] = 5
	new_path["Color"] = Color.ROYAL_BLUE
	new_path["AA"] = false #Anti aliasing
	new_path["Closed"] = false
	
	new_path.Path.curve = Curve2D.new()
	new_path.Path.name = str(child.Id)
	
	#Add the new path to the scenetree, and the global paths variable
	PathCont.add_child(new_path.Path)
	Global.Paths[child.Id] = new_path
	tree.add_child(child)

#Method that controls the visibility of the handles when a path is selected
func _on_path_selected(id: int) -> void:
	#When NONE (-1) is selected
	if id == -1:
		if editing_path != null:
			editing_path.hide()
			editing_path = null
		return
	
	#Only hide the current path if it actually exists
	if editing_path != null:
		editing_path.hide()
	
	#Set the editing_path to the selected one and show its nodes
	editing_path = PathCont.get_node_or_null(str(id))
	editing_path.show()
	editing_id = id


#Method that loads the custom paths when a file is loaded
func LoadCustomPaths() -> void:
	#Loop through paths
	for i in Global.Paths:
		var customPath = Global.Paths[i].Path
		var customCurve = customPath.curve
		customPath.name = str(i)
		
		#Instance a tree item in the editor
		var tree_item = line_scene.instantiate()
		tree_item.name = str(i)
		
		#Connect the signals to this script
		tree_item.VisibilityToggled.connect(_on_visibility_toggled)
		tree_item.DeletePressed.connect(_on_delete_pressed)
		tree_item.Selected.connect(_on_path_selected)
		tree_item.MetaDataChanged.connect(UpdatePaths)
		
		tree.add_child(tree_item)
		
		#Path instancing
		#Loop through points in path
		for j in customCurve.get_point_count():
			var handle = handle_scene.instantiate()
			handle.name = str(j)
			handle.global_position = customCurve.get_point_position(j)
			
			ConnectHandleSignals(handle)
			
			#Set curve handle positions
			var in_handle = handle.get_node("in")
			var out_handle = handle.get_node("out")
			in_handle.position = customCurve.get_point_in(j)
			out_handle.position = customCurve.get_point_out(j)
			
			customPath.add_child(handle)
			
			if j == customCurve.get_point_count()-1 and Global.Paths[i].Closed:
				handle.hide()
		
		PathCont.add_child(customPath)
	
	for i in Global.Paths:
		var item = tree.get_node(str(i))
		item.set_metadata(i)
	
	UpdatePaths()
	tree.get_child(0).select()
	
	$Inspector/vbox/CurveTools/Visibility.button_pressed = true

#Connect the signals from and to this script
func ConnectHandleSignals(handle) -> void:
	handle.pos_changed.connect(_on_handle_pos_changed)
	handle.in_pos_changed.connect(_on_in_handle_pos_changed)
	handle.out_pos_changed.connect(_on_out_handle_pos_changed)
	handle.handle_deleted.connect(_on_handle_deleted)
	handle.confirm_handle_delete.connect(_on_confirm_deletion)
	StateChanged.connect(handle._on_state_changed)

func get_last_point() -> Node:
	var last_idx = editing_path.get_child_count()-1
	return editing_path.get_child(last_idx)

#Method that pops up the delete confirm window
var deleting_id: int
func _on_delete_pressed(id: int) -> void:
	$Center/DeleteConfirm.show()
	deleting_id = id

func _on_delete_cancel_pressed() -> void:
	$Center/DeleteConfirm.hide()

#Method that handles path deleting
func _on_delete_confirm_pressed() -> void:
	$Center/DeleteConfirm.hide()
	
	#Delete the data about the path
	tree.get_node(str(deleting_id)).queue_free()
	PathCont.get_node(str(deleting_id)).queue_free()
	Global.Paths.erase(deleting_id)
	
	#Select the top item
	if PathCont.get_child_count() > 0:
		tree.get_child(0).select()
	
	UpdatePaths()

#Method to set the state from other scipts
func set_state(State) -> void:
	state = State
	emit_signal("StateChanged", state)

func _on_new_path_button_up() -> void:
	$Inspector/vbox/NewPath.release_focus()
	CreateTreeItem()


#Methods that handle the new locations when a handle is moved

func _on_handle_pos_changed(idx: int, pos: Vector2) -> void:
	editing_path.curve.set_point_position(idx, pos)
	
	if idx == 0 and Global.Paths[editing_id].Closed:
		var last_point = get_last_point()
		last_point.position = pos
		editing_path.curve.set_point_position(last_point.index, pos)
		
	UpdatePaths()

func _on_in_handle_pos_changed(idx: int, pos: Vector2) -> void:
	editing_path.curve.set_point_in(idx, pos)
	if Global.Paths[editing_id].Closed and idx == 0:
		var last_idx = get_last_point().index
		editing_path.curve.set_point_in(last_idx, pos)
		
	UpdatePaths()

func _on_out_handle_pos_changed(idx: int, pos: Vector2) -> void:
	editing_path.curve.set_point_out(idx, pos)
	UpdatePaths()

func _on_handle_deleted(idx: int) -> void:
	editing_path.curve.remove_point(idx)
	while idx < editing_path.get_child_count() - 1:
		var point = editing_path.get_node(str(idx+1))
		point.index = idx
		point.name = str(idx)
		idx+=1
	
	UpdatePaths()

#Method for setting visibility
func _on_visibility_toggled(id: int, toggled: bool) -> void:
	PathCont.get_node(str(id)).visible = !toggled
	Global.Paths[id].Visible = !toggled
	UpdatePaths()


#Connections for the toolbuttons and options

func _on_close_path_pressed() -> void:
	var toggled = !Global.Paths[editing_id].Closed
	Global.Paths[editing_id].Closed = toggled
	
	if toggled:
		var start_pos = editing_path.get_child(0).position
		var new_handle = CreateHandle(start_pos)
		new_handle.hide()
	
	else:
		var last_point = get_last_point()
		editing_path.curve.remove_point(last_point.index)
		last_point.queue_free()
	
	UpdatePaths()

func _on_none_pressed() -> void:
	set_state(NONE)

func _on_edit_pressed() -> void:
	set_state(MOVE)

func _on_add_pressed() -> void:
	set_state(ADD)

func _on_delete_tool_pressed() -> void:
	set_state(DELETE)

func _on_curve_pressed() -> void:
	set_state(CURVE)

func _on_mirror_mode_toggled(toggled: bool) -> void:
	for node in get_tree().get_nodes_in_group("curve_nodes"):
		node.mirror = toggled

func _on_curve_lines_toggled(toggled: bool) -> void:
	for node in get_tree().get_nodes_in_group("curve_nodes"):
		node.set_line_visibility(toggled)

func _on_option_toggle_pressed() -> void:
	$Inspector/vbox/CurveLines.visible = !$Inspector/vbox/CurveLines.visible
	$Inspector/vbox/MirrorMode.visible = !$Inspector/vbox/MirrorMode.visible

func _on_all_visibility_toggled(toggled: bool) -> void:
	PathCont.render = !toggled
	if !toggled:
		$Inspector/vbox/CurveTools/Visibility.icon = eye_icon
	if toggled:
		$Inspector/vbox/CurveTools/Visibility.icon = eye_slash_icon
	
	UpdatePaths()

func _on_confirm_deletion(idx: int) -> void:
	$Center/DeleteHandle.show()
	deleting_id = idx

func _on_handle_delete_cancel_pressed() -> void:
	$Center/DeleteHandle.hide()

func _on_handle_delete_pressed() -> void:
	PathCont.get_node(str(editing_id)).get_node(str(deleting_id)).DeleteHandle()
	$Center/DeleteHandle.hide()
