extends Label

func set_pos(pos: Vector2) -> void:
	text = str(pos)

func _on_pick_button_up() -> void:
	hide()

func _on_pick_button_down() -> void:
	show()
