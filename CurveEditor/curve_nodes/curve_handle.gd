extends Node2D

var pressed: bool

func _process(_delta: float) -> void:
	if pressed:
		global_position = get_global_mouse_position().floor()
		get_parent().set_handle_pos(name)
		$PosViewer.text = str(global_position)

func _on_pick_button_up() -> void:
	pressed = false

func _on_pick_button_down() -> void:
	pressed = true


