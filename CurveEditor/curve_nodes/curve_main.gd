extends Node2D

enum {NONE = -1, MOVE, ADD, DELETE, CURVE}

var state
var mirror: bool = false
var ShowCurveLines: bool = true

signal pos_changed(idx: int, pos: Vector2)
signal in_pos_changed(idx: int, pos: Vector2)
signal out_pos_changed(idx: int, pos: Vector2)
signal handle_deleted(idx: int)
signal confirm_handle_delete(idx: int)

var index: int
var pressed: bool
var mouse_offset: Vector2

func _draw() -> void:
	if state == CURVE and ShowCurveLines:
		draw_line(Vector2.ZERO, $in.position, Color.AQUA, 2)
		draw_line(Vector2.ZERO, $out.position, Color.RED, 2)

func _enter_tree() -> void:
	index = str(name).to_int()

func _ready() -> void:
	$PosViewer.text = str(global_position)

func _process(_delta: float) -> void:
	if !pressed: return
	
	if state == MOVE or state == ADD or state == CURVE:
		var mouse_pos := get_global_mouse_position().floor()
		global_position = mouse_pos - mouse_offset
		emit_signal("pos_changed", index, position)
		$PosViewer.text = str(global_position)
	
	if state == DELETE:
		emit_signal("confirm_handle_delete", index)
	

func set_handle_pos(Name: String) -> void:
	if Name == "in":
		if mirror:
			$out.position = -$in.position
			emit_signal("out_pos_changed", index, $out.position)
		
		emit_signal("in_pos_changed", index, $in.position)
		
	elif Name == "out":
		if mirror:
			$in.position = -$out.position
			emit_signal("in_pos_changed", index, $in.position)
		
		emit_signal("out_pos_changed", index, $out.position)
	
	update()

func _on_state_changed(State) -> void:
	state = State
	match State:
		NONE:
			hide_handles()
		MOVE:
			hide_handles()
			$pick.button_mask = MOUSE_BUTTON_MASK_LEFT | MOUSE_BUTTON_MASK_RIGHT
		ADD:
			hide_handles()
			$pick.button_mask = MOUSE_BUTTON_MASK_RIGHT
		DELETE:
			hide_handles()
			$pick.button_mask = MOUSE_BUTTON_MASK_LEFT
		CURVE:
			show_handles()
			$pick.button_mask = MOUSE_BUTTON_MASK_RIGHT

func show_handles() -> void:
	update()
	$in.show()
	$out.show()

func hide_handles() -> void:
	update()
	$in.hide()
	$out.hide()

func _on_pick_button_down() -> void:
	pressed = true
	if state == MOVE or state == ADD or state == CURVE:
		modulate.r = 0.6
		mouse_offset = get_global_mouse_position().floor() - global_position

func _on_pick_button_up() -> void:
	pressed = false
	modulate.r = 1

func set_line_visibility(toggled: bool) -> void:
	ShowCurveLines = toggled
	update()

func DeleteHandle() -> void:
	name = "[DELETED]"
	emit_signal("handle_deleted", index)
	queue_free()
