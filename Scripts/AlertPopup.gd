extends PanelContainer

var stylebox := get_theme_stylebox("panel")

@onready var iconSucces = load("res://Images/succes.png")
@onready var iconWarning = load("res://Images/warning.png")
@onready var iconError = load("res://Images/error.png")
@onready var iconInfo = load("res://Images/info.png")

@onready var icon = $hContainer/IconMargin/IconAlert
@onready var cross = $hContainer/CloseMargin/CloseButton
@onready var prog = $hContainer/CloseMargin/ProgressIndicator

const greenDark := Color("3ac279")
const greenLight := Color("c5f7dc")
const yellowDark := Color("e89f29")
const yellowLight := Color("ffe8c3")
const redDark := Color("e9594c")
const redLight := Color("ffcfcb")
const blueDark := Color("3d84e5")
const blueLight := Color("cde2ff")

var Type: Global.AlertType

func _ready() -> void:
	$AnimPlayer.play("Show")
	set_type(Type)
	add_theme_stylebox_override("panel", stylebox)

func set_succes() -> void:
	set_title("Succes")
	icon.texture = iconSucces
	icon.modulate = greenDark
	cross.modulate = greenDark
	stylebox.bg_color = greenLight
	stylebox.border_color = greenDark
	prog.tint_progress = greenDark
	
func set_warning() -> void:
	set_title("Warning")
	icon.texture = iconWarning
	icon.modulate = yellowDark
	cross.modulate = yellowDark
	stylebox.bg_color = yellowLight
	stylebox.border_color = yellowDark
	prog.tint_progress = yellowDark

func set_error() -> void:
	set_title("Error")
	icon.texture = iconError
	icon.modulate = redDark
	cross.modulate = redDark
	stylebox.bg_color = redLight
	stylebox.border_color = redDark
	prog.tint_progress = redDark

func set_info() -> void:
	set_title("Info")
	icon.texture = iconInfo
	icon.modulate = blueDark
	cross.modulate = blueDark
	stylebox.bg_color = blueLight
	stylebox.border_color = blueDark
	prog.tint_progress = blueDark

func set_title(title: String) -> void:
	$hContainer/TextMargin/TextContainer/Title.text = title

func set_type(type: Global.AlertType) -> void:
	match type:
		Global.AlertType.SUCCES:
			set_succes()
		Global.AlertType.WARNING:
			set_warning()
		Global.AlertType.ERROR:
			set_error()
		Global.AlertType.INFO:
			set_info()

func set_msg(msg: String) -> void:
	$hContainer/TextMargin/TextContainer/WarningMessage.text = msg

func _on_anim_player_animation_finished(_anim_name: StringName) -> void:
	queue_free()

func _on_close_button_up() -> void:
	$AnimPlayer.play("Close")











