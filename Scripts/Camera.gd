extends Camera2D

#Default values for camera
#Please edit these by clicking camera node in scene tree, then going to property window
@export var cam_speed := 1.0
@export var min_zoom := 0.1
@export var max_zoom := 5.0
@export var zoom_speed := 1.0
@export var zoom_factor := 0.1
@export var zoom_duration := 0.2
@export var cam_speed_curve: Curve
@export var zoom_speed_curve: Curve


#-----CAMERA MOVEMENT-------
var canPan = true
var isPanning = false
var scroll = true

var zoom_lvl := 1.0
var speed_mult := 1.0


func _ready():
	Global.GetCamera.connect(SetCamPos)
	set_zoom_level(1.0)

func disableScrolling():
	scroll = false
func enableScrolling():
	scroll = true

func pressed(action):
	return Input.is_action_pressed(action)

func _process(_delta):
	isPanning = Input.is_action_pressed("LMB") or Input.is_action_pressed("MMB")
	

func _unhandled_input(event: InputEvent) -> void:
	if isPanning:
		if event is InputEventMouseMotion:
			global_position += -event.relative * cam_speed * speed_mult
	if scroll:
		if event is InputEventMouseButton:
			if event.button_index == MOUSE_BUTTON_WHEEL_UP:
				zoom_lvl += zoom_factor * zoom_speed
				zoom_lvl = clamp(zoom_lvl, min_zoom, max_zoom)
				set_zoom_level(zoom_lvl)
				set_speed_mult(zoom_lvl)
			if event.button_index == MOUSE_BUTTON_WHEEL_DOWN:
				zoom_lvl -= zoom_factor * zoom_speed
				zoom_lvl = clamp(zoom_lvl, min_zoom, max_zoom)
				set_zoom_level(zoom_lvl)
				set_speed_mult(zoom_lvl)



func set_speed_mult(value: float) -> void:
	var i := range_lerp(value, min_zoom, max_zoom, 0.0, 1.0)
	speed_mult = cam_speed_curve.interpolate(i)

func set_zoom_level(value: float) -> void:
	var i := range_lerp(value, min_zoom, max_zoom, 0.0, 1.0)
	var j := zoom_speed_curve.interpolate(i)
	j = range_lerp(j, 0.0, 1.0, min_zoom, max_zoom)
	var t = create_tween().set_ease(Tween.EASE_OUT).set_trans(Tween.TRANS_SINE)
	t.tween_property(self, "zoom", Vector2(j, j), zoom_duration)

func SetCamPos(pos: Vector2):
	position = pos
