extends Control

@export_node_path(Node) var nodeContainer

var DataList: Array = []

func ViewData() -> void:
	var NodeContainer = get_node(nodeContainer)
	
	if Global.fileTypeLoaded == Global.FileType.NONE:
		$View.text = ""
		Global.SendAlert("No data to show!", Global.AlertType.WARNING)
		return
	
	DataList.clear()
	for i in NodeContainer.get_child_count():
		var child = NodeContainer.get_node(str(i))
		
		DataList.append(child.value)
	if Global.fileTypeLoaded == Global.FileType.GIMP:
		DataList.append(DataList.front())
	DataList.append(DataList.front())
	$View.text = str(DataList)
	show()
	
	
func _on_close_button_up() -> void:
	hide()
	
