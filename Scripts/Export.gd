extends PanelContainer

@onready var selection = $VContainer/SelectionHBox/PathSelection

var ids: Array = []
var export_id: int
var export_path: String
var export_dir: String = Settings.defaultExportFolder
var file_name: String
var confirmed: bool = false
var extension: String

func _init() -> void:
	Settings.set_paths.connect(_on_set_paths)
func _ready() -> void:
	$VContainer/EPLabel.text = "(" + export_dir + "...)"

func _on_export_popup_visibility_changed() -> void:
	$VContainer/Title.text = "Export as " + extension.right(3)

func _on_export_pressed() -> void:
	export_path = export_dir + file_name + extension
	var data: Dictionary = Global.Paths[export_id]
	
	var f := File.new()
	if f.file_exists(export_path) and !confirmed:
		$ExportConfirm.show()
		$VContainer.hide()
		return
	
	var export_string: String
	if extension == ".svg":
		export_string = get_svg_from_path()
	if extension == ".txt":
		export_string = get_short_svg_from_path()

	f.open(export_path, File.WRITE)
	f.store_string(export_string)
	f.close()
	
	confirmed = false
	hide()
	Global.SendAlert("Exported '" + data.Name + "'", Global.AlertType.SUCCES)

func _on_path_selection_item_selected(index: int) -> void:
	if index == 0:
		$VContainer/HBoxContainer2/Export.disabled = true
	else:
		$VContainer/HBoxContainer2/Export.disabled = false
		export_id = ids[index - 1]
		var str_name = selection.get_item_text(index)
		$VContainer/ExportPath/fileName.set_text(str_name)
		_on_export_path_text_changed(str_name)

func _on_export_browse_pressed() -> void:
	$SelectExport.popup_centered()

func _on_select_export_dir_selected(path: String) -> void:
	export_dir = path

func _on_export_path_text_changed(new_text: String) -> void:
	file_name = new_text
	
	export_path = export_dir + file_name + extension
	$VContainer/EPLabel.text = "("+ export_path +")"

func get_paths() -> void:
	
	
	selection.clear()
	selection.add_item("None", 0)
	var i = 1
	ids = []
	for id in Global.Paths:
		ids.append(id)
		selection.add_item(Global.Paths[id].Name, i)
		i += 1

#For setting the popup position
func _on_path_selection_pressed() -> void:
	var popup = selection.get_popup()
	var pos = Vector2(selection.global_position.x,
	selection.global_position.y + selection.size.y)
	popup.position = pos
	
func _on_set_paths() -> void:
	$SelectExport.current_dir = Settings.defaultExportFolder

func _on_close_pressed() -> void:
	hide()

func _on_confirm_pressed() -> void:
	confirmed = true
	_on_export_pressed()
	$VContainer.show()

func _on_cancel_pressed() -> void:
	$ExportConfirm.hide()
	$VContainer.show()

func curve_to_svg_string(curve: Curve2D, stroke_width: float, stroke_color: Color) -> String:
	var n := curve.get_point_count()

	var bounds := Rect2(curve.get_point_position(0), Vector2.ZERO)
	for point in curve.get_baked_points():
		bounds = bounds.expand(point)
	bounds = bounds.grow(stroke_width)

	var data_string := ''

	var first_point := curve.get_point_position(0)
	data_string += "M%f %f" % [int(first_point.x), int(first_point.y)]

	for i in range(1, n):
		var from := curve.get_point_position(i - 1)
		var from_out := from + curve.get_point_out(i - 1)
		var to := curve.get_point_position(i)
		var to_in := to + curve.get_point_in(i)
		
		from = Vector2(int(from.x), int(from.y))
		from_out = Vector2(int(from_out.x), int(from_out.y))
		to = Vector2(int(to.x), int(to.y))
		to_in = Vector2(int(to_in.x), int(to_in.y))

		if from == from_out and to == to_in:
			# Straight line.
			data_string += "L%f %f" % [
				to.x, to.y,
			]
		else:
			# Cubic Bezier.
			data_string += "C%f %f %f %f %f %f" % [
				from_out.x, from_out.y,
				to_in.x, to_in.y,
				to.x, to.y,
			]
			

	var path_string := '<path d="%s" fill="none" stroke="#%s" stroke-width="%f"/>' % [
		data_string, stroke_color.to_html(false), stroke_width,
	]

	var svg_string := '<svg width="%f" height="%f" viewBox="%f %f %f %f" xmlns="http://www.w3.org/2000/svg">%s</svg>' % [
		# width, height
		bounds.size.x, bounds.size.y,

		# viewBox
		0, 0,
		Global.TrackSize.x, Global.TrackSize.y,
#		bounds.position.x, bounds.position.y,
#		bounds.size.x, bounds.size.y,

		# content
		path_string,
	]

	return svg_string

func get_short_svg_from_path() -> String:
	var path: Dictionary = Global.Paths[export_id]
	var curve: Curve2D = path.Path.curve
	var closed: bool = path.Closed
	
	var data_start := '"M %d,%d C ' % [
				#start.x, start.y
				curve.get_point_position(0).x, curve.get_point_position(0).y
			]
	var data := ''
	
	for i in curve.get_point_count():
		var main := curve.get_point_position(i)
		var h_in := curve.get_point_in(i) + main
		var h_out := curve.get_point_out(i) + main
		var point_string := ''
		var last := false
		if i == curve.get_point_count()-1:
			last = true
		
		if i == 0:
			point_string = '%d,%d' % [
				main.x, main.y
			]
			
		else:
			if !last:
				point_string = ' %d,%d %d,%d %d,%d' % [
				h_in.x, h_in.y,
				main.x, main.y,
				h_out.x, h_out.y
			]
			else:
				point_string = ' %d,%d %d,%d' % [
				h_in.x, h_in.y,
				main.x, main.y
			]
		data += point_string

	if closed:
		data += ' Z"'
	else:
		data += '"'
	
	data = data_start + data
	
	return data

func get_svg_from_path() -> String:
	var path: Dictionary = Global.Paths[export_id]
	var curve: Curve2D = path.Path.curve
	var closed: bool = path.Closed
	var stroke_width: int = path.Width
	var stroke_color: Color = path.Color
	
	var svg_string := '<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 20010904//EN"
			  "http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd">\n
<svg xmlns="http://www.w3.org/2000/svg"\n'
	
	var metadata := '     width="%d" height="%d"
	 viewBox="0 0 %d %d">
  <path id="%s" fill="none" stroke="#%s" stroke-width="%d"\n' % [
	#width, height
	Global.TrackSize.x, Global.TrackSize.y,
	#viewBox
	Global.TrackSize.x, Global.TrackSize.y,
	#name
	path.Name,
	#stroke, stroke_width 
	stroke_color.to_html(false), stroke_width
	]
	svg_string += metadata
	
	var data_start := 'd="M %d,%d C ' % [
				#start.x, start.y
				curve.get_point_position(0).x, curve.get_point_position(0).y
			]
	var data := ''
	
	for i in curve.get_point_count():
		var main := curve.get_point_position(i)
		var h_in := curve.get_point_in(i) + main
		var h_out := curve.get_point_out(i) + main
		var point_string := ''
		var last := false
		if i == curve.get_point_count()-1:
			last = true
		
		if i == 0:
			point_string = '%d,%d' % [
				main.x, main.y
			]
			
		else:
			if !last:
				point_string = ' %d,%d %d,%d %d,%d' % [
				h_in.x, h_in.y,
				main.x, main.y,
				h_out.x, h_out.y
			]
			else:
				point_string = ' %d,%d %d,%d' % [
				h_in.x, h_in.y,
				main.x, main.y
			]
		data += point_string

	if closed:
		data += ' Z" />
</svg>'
	else:
		data += '" />
</svg>'
	
	data = data_start + data
	svg_string += data
	
	return svg_string
#<?xml version="1.0" encoding="UTF-8" standalone="no"?>
#<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 20010904//EN"
#              "http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd">
#<svg xmlns="http://www.w3.org/2000/svg"

#     width="image_size.x" height="image_size.y"
#     viewBox="0 0 image_size.x image_size.y">

#<path id="path" fill="none" stroke="path_color" stroke-width="path_width"
#d="M [first.x,first.y]
#   C [first.x,first.y] [in.x,in.y] [main.x,main.y]
#	  [out.x,out.y] [in.x,in.y] [main.x,main.y]
#	  [out.x,out.y] [REPEAT
#				  ] [last_in.x,last_in.y] [last.x,last.y][Z if closed]" />
#</svg>







