extends Node

@onready var VERSION: String = ProjectSettings.get_setting("application/config/version")

signal CameraMoved
signal Alert(msg: String)
signal GetCamera(pos: Vector2)
signal SetTrack(path: String)
signal XMLPasted(path: String)
signal XMLParsed

enum AlertType {SUCCES, WARNING, ERROR, INFO}

enum FileType{SVG, GIMP, NONE = -1}
var fileTypeLoaded: FileType = FileType.NONE
var TrackImage: String
var TrackSize: Vector2
var SVGFile: String
var Paths: Dictionary = {}

var basePath := OS.get_executable_path().get_base_dir()
var saveFolder := basePath + "/Saves/"
var exportFolder := basePath + "/Exports/"
var recoveryFile := saveFolder + "RecoveryFile.tmgpro"

@onready var nodeContainer = get_node_or_null("../Main/NrEditors")
@onready var alertContainer = get_node_or_null("../Main/UILayer/AlertContainer/Center")
@onready var curveEditor = get_node_or_null("../Main/UILayer/UI/ToolbarContainer/CurveEditor")

@onready var NrEditor := load("res://Scenes/nr_editor.tscn")
@onready var AlertScene := load("res://Scenes/alert_popup.tscn")

func _ready() -> void:
	#Make the save folder if it doesn't exists
	var dir = Directory.new()
	if !dir.dir_exists(saveFolder):
		dir.make_dir(saveFolder)
	if !dir.dir_exists(exportFolder):
		dir.make_dir(exportFolder)

func Load(path: String) -> void:
	#Check if the selected file is a save file we can read
	if !path.ends_with("tmgpro"):
		SendAlert("Unsupported file type", AlertType.ERROR)
		return
	
	#Check if something is already loaded
	if fileTypeLoaded != FileType.NONE:
		SendAlert("Please clear the current track before loading another one", AlertType.WARNING)
		return
	
	#Check if the file exists
	var file = File.new()
	if !file.file_exists(path):
		SendAlert("Could not find file: "+Array(path.split("/")).back(), AlertType.ERROR)
		return
	
	#Read and store the data from save file in 'data' variable
	file.open(path, File.READ)
	var data = file.get_as_text()
	data = str2var(data)
	file.close()
	
	#Check for what version it's from
	if !data.has("version"):
		load_v3_0(data)
	elif data.version == "3.0":
		load_v3_0(data)
	elif data.version == "4.0":
		load_v4_0(data)
	else:
		SendAlert("Unsupported save file version", AlertType.ERROR)
		return
	
	#Alert user file has been loaded
	SendAlert("Loaded " + Array(path.split("/")).back(), AlertType.SUCCES)

func Recover() -> void:
	Load(recoveryFile)

func Save(path: String) -> void:
	var file := File.new()
	file.open(path, File.WRITE)
	var tmp = CompileSaveData()
	var data = var2str(tmp)
	file.store_line(data)
	file.close()
	SendAlert("File saved to "+ Array(path.split("/")).back(), AlertType.INFO)

func QuickSave() -> void:
	var file := File.new()
	file.open(recoveryFile, File.WRITE)
	var tmp = CompileSaveData()
	var data = var2str(tmp)
	file.store_line(data)
	file.close()

	
func set_values(values: Array) -> void:
	var hadError := false
	for child in nodeContainer.get_children():
		if !has_valid_values(values):
			SendAlert("Not all nodes got a value, array contained too few values", AlertType.WARNING)
			return
		
		var intName = str(child.name).to_int()
		if intName <= values.size()-1:
			child.set_value(values[intName])
			values[intName] = -1
		else:
			hadError = true
	if hadError:
		SendAlert("Not all nodes got a value, array contained too few values", AlertType.WARNING)
	
	if has_valid_values(values):
		SendAlert("Not all values could be assigned: too many values in array", AlertType.WARNING)
	elif !hadError:
		SendAlert("Imported array of values", AlertType.SUCCES)

func has_valid_values(values: Array) -> bool:
	for value in values:
		if value != -1:
			return true
	return false

#Clear data, track and custom paths
func ClearAll() -> void:
	#Clear track
	emit_signal("SetTrack", "None")
	
	#Clear nodes
	for child in nodeContainer.get_children():
		child.queue_free()
	
	#Clear paths
	var path_container = curveEditor.PathCont
	var tree = curveEditor.tree
	
	for item in tree.get_children():
		if item.name != "None":
			item.queue_free()
	for path in path_container.get_children():
		path.queue_free()
	
	#Reset variables
	fileTypeLoaded = FileType.NONE
	SVGFile = ""
	TrackImage = ""
	Paths = {}
	path_container.update()
	
	emit_signal("GetCamera", Vector2.ZERO)
	SendAlert("Cleared the canvas\nUse recover if this was not intentional", AlertType.INFO)

#Clear user created paths
func ClearCustomPaths() -> void:
	var path_container = curveEditor.PathCont
	var tree = curveEditor.tree
	
	for item in tree.get_children():
		if item.name != "None":
			item.queue_free()
	for path in path_container.get_children():
		path.queue_free()
	Paths = {}
	path_container.update()
	SendAlert("Cleared all paths\nUse recover if this was not intentional", AlertType.INFO)

#Clear the track image
func ClearTrack() -> void:
	emit_signal("SetTrack", "None")
	SendAlert("Cleared track image", Global.AlertType.INFO)
	TrackImage = ""

#Clear the number editor nodes
func ClearData() -> void:
	QuickSave()
	for child in nodeContainer.get_children():
		child.queue_free()
	SendAlert("Cleared all nodes\nUse recover if this was not intentional", AlertType.INFO)
	fileTypeLoaded = FileType.NONE
	SVGFile = ""

#Method to send alerts to the user
func SendAlert(msg: String, type: AlertType) -> void:
	var alert = AlertScene.instantiate()
	alert.Type = type
	alert.set_msg(msg)
	alertContainer.add_child(alert)

#Gather the all data that needs to be saved
func CompileSaveData() -> Dictionary:
	var saveDict: Dictionary = {}
	var nodeDict: Dictionary = {}
	for node in nodeContainer.get_children():
		var nodeData: Dictionary = {}
		nodeData["pos"] = node.position
		nodeData["value"] = node.get_value()
		nodeData["focus"] = node.get_focus_neighbours()
		nodeDict[node.name] = nodeData
	
	saveDict["nodes"] = nodeDict
	saveDict["fileType"] = fileTypeLoaded
	saveDict["track"] = TrackImage
	saveDict["customPaths"] = Paths
	saveDict["version"] = VERSION
	return saveDict

#Load files from version 4.0
func load_v4_0(data: Dictionary) -> void:
	load_v3_0(data)
	Paths = data.customPaths
	curveEditor.LoadCustomPaths()

#Load files from version 3.0
func load_v3_0(data: Dictionary) -> void:
	fileTypeLoaded = data.fileType
	emit_signal("SetTrack", data.track)
	
	var nodeData = data.nodes
	for i in nodeData.keys():
		var newNode = NrEditor.instantiate()
		newNode.name = i
		newNode.position = nodeData[i].pos
		nodeContainer.add_child(newNode)
		newNode.set_value(nodeData[i].value)
	
	for child in nodeContainer.get_children():
		child.set_focus_neighbours(nodeData[child.name].focus)

