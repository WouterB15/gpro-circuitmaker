extends Node2D

var parser := XMLParser.new()
var NrEditor := preload("res://Scenes/nr_editor.tscn")

var content: Array = []

func _ready() -> void:
	Global.XMLPasted.connect(_on_track_data_selected)

func _on_track_data_selected(path: String) -> void:
	if Global.fileTypeLoaded != Global.FileType.NONE:
		Global.SendAlert("Clear nodes before importing new data", Global.AlertType.WARNING)
		return
	
	Global.SVGFile = path
	content = []
	read_file(path)
	
	Global.SendAlert("Succesfully imported "+Array(path.split("/")).back(), Global.AlertType.SUCCES)
	Global.emit_signal("XMLParsed")

func read_file(path: String) -> void:
	var result: String
	parser.open(path)
	while parser.read() != ERR_FILE_EOF:
		if parser.get_attribute_count() > 0:
			for i in range(parser.get_attribute_count()):
				if parser.get_attribute_name(i) == "d":
					result = parser.get_attribute_value(i)
					Global.fileTypeLoaded = Global.FileType.GIMP
		
		if parser.get_node_name().contains("circle"):
			var NodePos := Vector2.ZERO
			NodePos.x = parser.get_attribute_value(0).to_int()
			NodePos.y = parser.get_attribute_value(1).to_int()
			content.append(NodePos)
			Global.fileTypeLoaded = Global.FileType.SVG
	
	if Global.fileTypeLoaded == Global.FileType.SVG:
		CreateNodes()
		return
	
	if result.contains(" Z"):
		result = result.split(" Z")[0]
	
	#Remove unnecessary content from string
	var data = Array(result.split("C "))[1]
	data = Array(data.split(" "))
	while data.has(""):
		var pos = data.find("")
		data.remove_at(pos)
	
	#Convert strings to Vector2's
	var tempArray: Array = []
	for location in data:
		var tmp = location.split(",")
		var pos = Vector2(tmp[0].to_int(), tmp[1].to_int())
		tempArray.append(pos)
	
	#Loop for oddly arranged coordinates and only use main node locations, not curve handles
	for i in range(tempArray.size()):
		if i == 0:
			content.append(tempArray[i])
		elif (i + 1) % 3 == 0:
			content.append(tempArray[i])
	content.remove_at(content.size()-1)
	CreateNodes()



func CreateNodes() -> void:
	DeleteNodes()
	
	var firstNode
	var prevNode
	
	var i: int = 0
	for pos in content:
		var node = NrEditor.instantiate()
		node.position = pos
		node.name = str(i)
		add_child(node)
		if i == 0:
			firstNode = node
		else:
			node.get_child(0).get_line_edit().focus_previous = prevNode.get_child(0).get_line_edit().get_path()
			prevNode.get_child(0).get_line_edit().focus_next = node.get_child(0).get_line_edit().get_path()
		
		i+=1
		if content.size() == i:
			node.get_child(0).get_line_edit().focus_next = firstNode.get_child(0).get_line_edit().get_path()
			firstNode.get_child(0).get_line_edit().focus_previous = node.get_child(0).get_line_edit().get_path()
		
		prevNode = node

func DeleteNodes():
	for child in get_children():
		child.queue_free()








