extends MarginContainer

var path: String = ""

func _ready() -> void:
	Global.XMLParsed.connect(_delete_tmp)

func _on_submit_button_up() -> void:
	var file := File.new()
	var txt: String = $vBox/XMLWindow.text
	
	
	path = Global.basePath + "/tmp.svg"
	if txt.contains("circle"):
		txt = "<div>" + txt + "</div>"
	
	
	if str2var(txt) is Array:
		Global.set_values(str2var(txt))
		hide()
		return
	
	if path.ends_with(".tmp") and !txt.contains("<path"):
		Global.SendAlert("Could not read input", Global.AlertType.ERROR)
		hide()
		return
	
	file.open(path, File.WRITE)
	file.store_line(txt)
	file.close()
	Global.emit_signal("XMLPasted", path)
	hide()


func _on_cancel_button_up() -> void:
	hide()

func _delete_tmp():
	if path == "":
		return
	
	var dir = Directory.new()
	dir.open(Global.basePath)
	var _err = dir.remove(path)
	path = ""
		
