extends CenterContainer

func _enter_tree() -> void:
	Settings.set_paths.connect(set_default_paths)

func set_default_paths():
	$SavePopup.current_dir = Settings.defaultSaveLoadFolder
	$LoadPopup.current_dir = Settings.defaultSaveLoadFolder
	$ImportTrack.current_dir = Settings.defaultImportFolder
	$ImportTrackData.current_dir = Settings.defaultImportFolder

func _on_save_popup_file_selected(path: String) -> void:
	Global.Save(path)

func _on_load_popup_file_selected(path: String) -> void:
	Global.Load(path)
