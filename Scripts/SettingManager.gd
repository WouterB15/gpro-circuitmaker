extends PanelContainer


func _enter_tree() -> void:
	Settings.set_paths.connect(_on_set_paths)

func _on_set_paths() -> void:
	$VContainer/SaveLoad/DefaultSaveLoadPath.text = Settings.defaultSaveLoadFolder
	$VContainer/Import/DefaultImportPath.text = Settings.defaultImportFolder
	$VContainer/Export/defaultExportPath.text = Settings.defaultExportFolder
	$SelectImport.current_dir = Settings.defaultImportFolder
	$SelectSaveLoad.current_dir = Settings.defaultSaveLoadFolder
	$SelectExport.current_dir = Settings.defaultExportFolder

func _on_select_save_load_dir_selected(dir: String) -> void:
	Settings.defaultSaveLoadFolder = dir
	Settings.save_settings()

func _on_select_import_dir_selected(dir: String) -> void:
	Settings.defaultImportFolder = dir
	Settings.save_settings()

func _on_select_export_dir_selected(dir: String) -> void:
	Settings.defaultExportFolder = dir
	Settings.save_settings()

func _on_close_button_up() -> void:
	hide()

func _on_import_browse_button_up() -> void:
	$SelectImport.popup_centered()

func _on_sl_browse_button_up() -> void:
	$SelectSaveLoad.popup_centered()

func _on_export_browse_button_up() -> void:
	$SelectExport.popup_centered()

func _on_default_import_path_text_changed(new_text: String) -> void:
	Settings.defaultImportFolder = new_text
	Settings.save_settings()

func _on_default_save_load_path_text_changed(new_text: String) -> void:
	Settings.defaultSaveLoadFolder = new_text
	Settings.save_settings()

func _on_default_export_path_text_changed(new_text: String) -> void:
	Settings.defaultExportFolder = new_text
	Settings.save_settings()





