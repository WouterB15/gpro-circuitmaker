extends PanelContainer

signal set_paths
const def_filepaths_section: String = "Default Filepaths"

var defaultSaveLoadFolder: String = Global.saveFolder
var defaultImportFolder: String = Global.basePath
var defaultExportFolder: String = Global.exportFolder


var config = ConfigFile.new()

func _ready() -> void:
	load_settings()

func load_settings() -> void:
	emit_signal("set_paths")
	var err = config.load(Global.basePath+"/settings.cfg")
	if err != OK:
		return
	for section in config.get_sections():
		defaultImportFolder = config.get_value(def_filepaths_section, "Import", "def")
		defaultSaveLoadFolder = config.get_value(def_filepaths_section, "SaveLoad", "def")
		defaultExportFolder = config.get_value(def_filepaths_section, "Export", "def")
	
	emit_signal("set_paths")
	
func save_settings() -> void:
	emit_signal("set_paths")
	config.set_value(def_filepaths_section, "SaveLoad", defaultSaveLoadFolder)
	config.set_value(def_filepaths_section, "Import", defaultImportFolder)
	config.set_value(def_filepaths_section, "Export", defaultExportFolder)
	
	config.save(Global.basePath+"/settings.cfg")






