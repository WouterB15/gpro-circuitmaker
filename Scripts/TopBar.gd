extends Control


func _ready() -> void:
	$FileButton.get_popup().id_pressed.connect(FileOptionPressed)
	$FileButton.get_popup().visibility_changed.connect(_set_file_pos)
	
	$ImportButton.get_popup().id_pressed.connect(ImportOptionsPressed)
	$ImportButton.get_popup().visibility_changed.connect(_set_import_pos)
	
	$EditButton.get_popup().id_pressed.connect(EditPressed)
	$EditButton.get_popup().visibility_changed.connect(_set_edit_pos)
	
	$ExportButton.get_popup().id_pressed.connect(ExportPressed)
	$ExportButton.get_popup().visibility_changed.connect(_set_export_pos)
	
	$ViewButton.get_popup().id_pressed.connect(ViewPressed)
	$ViewButton.get_popup().visibility_changed.connect(_set_view_pos)

func EditPressed(id: int) -> void:
	Global.QuickSave()
	match id:
		0: #Clear track
			Global.ClearTrack()
		1: #Clear data
			Global.ClearData()
		2: #Clear all
			Global.ClearAll()
		3: #Clear paths
			Global.ClearCustomPaths()

func ViewPressed(id: int) -> void:
	$ViewButton.get_popup().toggle_item_checked(id)
	match id:
		0: #Show path window
			var curveEditor = get_node("../../CurveEditor")
			curveEditor.visible = !curveEditor.visible
		1: #Show nodes
			Global.nodeContainer.visible = !Global.nodeContainer.visible
		2: #Show white background
			var bg := get_node("../../../../../TrackImage/WhiteBG")
			bg.visible = !bg.visible

func ExportPressed(id: int) -> void:
	var export = get_node("../../../Popups/ExportPopup")
	export.get_paths()
	match id:
		0: #export as svg
			export.extension = ".svg"
			export.show()
		1: #export as txt
			export.extension = ".txt"
			export.show()
		2: #show values
			get_node("../../../Popups/DataPopup").ViewData()
			Global.QuickSave()

func FileOptionPressed(id : int) -> void:
	match id:
		0: #Save popup
			get_node("../../../Popups/SavePopup").popup_centered()
		1: #Load popup
			get_node("../../../Popups/LoadPopup").popup_centered()
		2: #Recover
			Global.Recover()
		3: #Settings
			get_node("../../../Popups/Settings").show()

func ImportOptionsPressed(id : int) -> void:
	match id:
		0: #Import track popup
			get_node("../../../Popups/ImportTrack").popup_centered()
		1: #Import track data popup
			get_node("../../../Popups/ImportTrackData").popup_centered()
		2: #Paste XML
			get_node("../../../PastePopup").show()
	


func _set_file_pos() -> void:
	var popup = $FileButton.get_popup()
	popup.position = Vector2(0, 31)

func _set_edit_pos() -> void:
	var popup = $EditButton.get_popup()
	popup.position = Vector2(80, 31)

func _set_import_pos() -> void:
	var popup = $ImportButton.get_popup()
	popup.position = Vector2(160, 31)

func _set_export_pos() -> void:
	var popup = $ExportButton.get_popup()
	popup.position = Vector2(240, 31)

func _set_view_pos() -> void:
	var popup = $ViewButton.get_popup()
	popup.position = Vector2(320, 31)
