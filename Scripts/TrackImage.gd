extends TextureRect

func _ready() -> void:
	Global.SetTrack.connect(set_track_image)

func _on_import_track_file_selected(path: String) -> void:
	set_track_image(path)
	Global.SendAlert("Set the track image", Global.AlertType.SUCCES)

func set_track_image(path: String) -> void:
	if path == "":
		Global.SendAlert("Could not load track image", Global.AlertType.ERROR)
		return
	if path == "None":
		texture = null
		return
	
	var image = Image.new()
	image.load(path)
	var track = ImageTexture.new()
	track.create_from_image(image)
	texture = track
	Global.TrackImage = path
	Global.TrackSize = texture.get_size()

