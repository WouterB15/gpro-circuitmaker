extends Sprite2D

var lineEdit : LineEdit
var value: float

const redDark := Color("e9594ca4")
const greenDark := Color("29ff006e")
const defaultColor := Color("7bffe06e")

func _ready() -> void:
	lineEdit = $SpinBox.get_line_edit()
	lineEdit.max_length = 3
	lineEdit.placeholder_text = "000"
	lineEdit.focus_entered.connect(_on_got_focus)
	lineEdit.focus_exited.connect(_on_lost_focus)
	lineEdit.flat = true
	if lineEdit.text == "0":
		lineEdit.text = ""
	$AnimPlayer.play("Hide")
	
	lineEdit.text_changed.connect(_text_changed)

func _input(event: InputEvent) -> void:
	if lineEdit.has_focus():
		if event.is_action_pressed("SHIFT"):
			$SpinBox.editable = false
		if event.is_action_pressed("add"):
			$SpinBox.value += 5
		if event.is_action_pressed("subtract"):
			$SpinBox.value -= 5
	if event.is_action_released("SHIFT"):
		$SpinBox.editable = true

func get_value() -> float:
	return $SpinBox.value

func set_value(nr: float) -> void:
	$SpinBox.value = nr 

func set_focus_neighbours(focus: Array) -> void:
	var container = get_parent()
	var path_prev = str(focus[0]).split("NrEditors/")[1]
	var path_next = str(focus[1]).split("NrEditors/")[1]
	
	lineEdit.focus_previous = container.get_node(path_prev).get_line_edit().get_path()
	lineEdit.focus_next = container.get_node(path_next).get_line_edit().get_path()

func get_focus_neighbours() -> Array:
	var n: Array = []
	n.append(get_node(lineEdit.focus_previous).get_parent().get_path())
	n.append(get_node(lineEdit.focus_next).get_parent().get_path())
	return n

func _text_changed(new_text: String) -> void:
	if new_text == "":
		self_modulate = defaultColor

func _on_spin_box_value_changed(nr: float) -> void:
	value = nr
	if Global.fileTypeLoaded != Global.FileType.SVG:
		return
	
	if nr == 0 or lineEdit.text == "":
		self_modulate = defaultColor
	if nr > 95:
		self_modulate = redDark
	elif nr <= 95 and nr > 0:
		self_modulate = greenDark

func _on_got_focus() -> void:
	lineEdit.select_all()
	Global.emit_signal("GetCamera", position)
	$AnimPlayer.play_backwards("Hide")
	Global.QuickSave()

func _on_lost_focus() -> void:
	$AnimPlayer.play("Hide")
	
