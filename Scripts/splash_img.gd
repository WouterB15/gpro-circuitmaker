extends Control

func _ready() -> void:
	show()
	get_tree().get_root().mode = Window.MODE_MAXIMIZED
	$AnimPlayer.play("boot")

func _on_anim_player_animation_finished(_anim_name: StringName) -> void:
	queue_free()
